﻿@ModelType IEnumerable(Of Final.Reserva)
@Code
ViewData("Title") = "Index"
End Code

<h2>Mis reservas</h2>

@*<p>
    @Html.ActionLink("Create New", "Create")
</p>*@
<table class="table">
    <tr>
        <th>
           Hotel
        </th>
        <th>
           A nombre de 
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Desde)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Hasta)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Total)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Habitaciones)
        </th>
        <th></th>
    </tr>

@For Each item In Model
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Hotel.Nombre)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Persona.Nombre)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Desde)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Hasta)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Total)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Habitaciones)
        </td>
        <td>
            @Html.ActionLink("Edit", "Edit", New With {.id = item.Id }) |
            @*@Html.ActionLink("Details", "Details", New With {.id = item.Id }) |*@
            @Html.ActionLink("Delete", "Delete", New With {.id = item.Id })
        </td>
    </tr>
Next

</table>
