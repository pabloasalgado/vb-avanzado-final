﻿@ModelType Final.Reserva
@Code
    ViewData("Title") = "Details"
End Code

<h2>Details</h2>

<div>
    <h4>Reserva</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.Hotel.Nombre)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Hotel.Nombre)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Persona.Nombre)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Persona.Nombre)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Desde)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Desde)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Hasta)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Hasta)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Total)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Total)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Habitaciones)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Habitaciones)
        </dd>

    </dl>
</div>
<p>
    @Html.ActionLink("Edit", "Edit", New With { .id = Model.Id }) |
    @Html.ActionLink("Back to List", "Index")
</p>
