﻿@ModelType Final.Reserva
@Code
    ViewData("Title") = "Reservas"
End Code

<h2>Crear</h2>

@Using (Html.BeginForm()) 
    @Html.AntiForgeryToken()

    @<div class="form-horizontal">
        <h4>Reserva</h4>
        <hr />
        @Html.ValidationSummary(True, "", New With {.class = "text-danger"})
        <div class="form-group">
            @*@Html.LabelFor(Function(model) model.PersonaId, "PersonaId", htmlAttributes:= New With { .class = "control-label col-md-2" })*@
            <label class="control-label col-md-2">A nombre de</label>
            <div class="col-md-10">
                @Html.HiddenFor(Function(model) model.PersonaId)
                @Html.DropDownList("PersonaId", Nothing, htmlAttributes:=New With {.class = "form-control", .disabled = "disabled"})
                @Html.ValidationMessageFor(Function(model) model.PersonaId, "", New With { .class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            @*@Html.LabelFor(Function(model) model.HotelId, "HotelId", htmlAttributes:= New With { .class = "control-label col-md-2" })*@
            <label class = "control-label col-md-2">Hotel</label>
            <div class="col-md-10">
                @Html.HiddenFor(Function(model) model.HotelId)
                @Html.DropDownList("HotelId", Nothing, htmlAttributes:=New With {.class = "form-control", .disabled = "disabled"})
                @Html.ValidationMessageFor(Function(model) model.HotelId, "", New With { .class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            @Html.LabelFor(Function(model) model.Desde, htmlAttributes:= New With { .class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(Function(model) model.Desde, New With { .htmlAttributes = New With { .class = "form-control" } })
                @Html.ValidationMessageFor(Function(model) model.Desde, "", New With { .class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            @Html.LabelFor(Function(model) model.Hasta, htmlAttributes:= New With { .class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(Function(model) model.Hasta, New With { .htmlAttributes = New With { .class = "form-control" } })
                @Html.ValidationMessageFor(Function(model) model.Hasta, "", New With { .class = "text-danger" })
            </div>
        </div>

        @*<div class="form-group">
            @Html.LabelFor(Function(model) model.Total, htmlAttributes:= New With { .class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(Function(model) model.Total, New With { .htmlAttributes = New With { .class = "form-control" } })
                @Html.ValidationMessageFor(Function(model) model.Total, "", New With { .class = "text-danger" })
            </div>
        </div>*@

        <div class="form-group">
            @Html.LabelFor(Function(model) model.Habitaciones, htmlAttributes:= New With { .class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(Function(model) model.Habitaciones, New With { .htmlAttributes = New With { .class = "form-control" } })
                @Html.ValidationMessageFor(Function(model) model.Habitaciones, "", New With { .class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Reservar" class="btn btn-default" />
            </div>
        </div>
    </div>
End Using

<div>
    @Html.ActionLink("Mis reservas", "Index")
</div>

@Section Scripts 
    @Scripts.Render("~/bundles/jqueryval")
End Section
