﻿@ModelType Final.Reserva
@Code
    ViewData("Title") = "Delete"
End Code

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<div>
    <h4>Reserva</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.Hotel.Nombre)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Hotel.Nombre)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Persona.Nombre)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Persona.Nombre)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Desde)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Desde)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Hasta)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Hasta)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Total)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Total)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Habitaciones)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Habitaciones)
        </dd>

    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
            <input type="submit" value="Delete" class="btn btn-default" /> |
            @Html.ActionLink("Back to List", "Index")
        </div>
    End Using
</div>
