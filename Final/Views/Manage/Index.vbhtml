﻿@ModelType IndexViewModel
@Code
    ViewBag.Title = "Mi cuenta"
End Code

<h2>@ViewBag.Title.</h2>

<p class="text-success">@ViewBag.StatusMessage</p>
<div>
    <h4>Modificar cuenta</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>Mi contraseña:</dt>
        <dd>
            [
            @If Model.HasPassword Then
                @Html.ActionLink("Cambiar contraseña", "ChangePassword")
            Else
                @Html.ActionLink("Crear contraseña", "SetPassword")
            End If
            ]
        </dd>
        <dt>Mis datos:</dt>
        <dd>[
            @Html.ActionLink("Modificar", "Edit", "Personas", New With {.id = Model.Persona.Id}, Nothing)
            ]
        </dd>
        <dt>Mis reservas:</dt>
        <dd>
            [
            @Html.ActionLink("Ver", "Index", "Reservas", Nothing, Nothing)
            ]
        </dd>
    </dl>
</div>
