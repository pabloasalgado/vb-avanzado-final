﻿@Code
    ViewData("Title") = "Acerca"
End Code

<h2>@ViewData("Title").</h2>
<h3>@ViewData("Message")</h3>

<p>Aplicación web  que permita consultar  sitios turísticos y hoteles  por región, en una base de datos para entorno web.</p>
 
<p>Maneja datos de hoteles, costo, lugar, especialidad,  país,  clasificación del hotel, características de los lugares.</p>
