﻿@ModelType IEnumerable(Of Sitio)
@Code
    ViewData("Title") = "Home Page"
End Code

<p></p>
@Using (Html.BeginForm("Index", "Home", FormMethod.Get))
    @Html.AntiForgeryToken

    @<div class="form-inline">
         <div class="form-group">
             <label class="sr-only" for="searchString">Buscar</label>
             <div class="input-group">
                 <div class="input-group-addon"><i class="glyphicon glyphicon-search"></i></div>
                 @Html.TextBox("searchString", Nothing, New With {.class = "form-control", .placeHolder = "Sitio turístico, ciudad ..."})
             </div>
         </div>
         <button type="submit" class="btn btn-primary">Buscar</button>   
     </div> End Using
    <br />
    <table class="table">
        <tr>
            <th>Nombre del sitio</th>
            <th>Ciudad</th>
            <th>País</th>
            <th></th>
            <th></th>
        </tr>
@For Each item In Model
        @<tr>
            <td>
                @Html.ActionLink(item.Nombre, "Details", "Sitios", New With {.id = item.Id}, Nothing)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.Ciudad.Nombre)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.Ciudad.Pais.Nombre)
            </td>
            <td>
                <img src="@Url.Content(item.FotoUrl)" style="width: 100px;" alt="@item.FotoAlt"/>
            </td>
            <td>
                <a href="@Url.Content(item.InformacionUrl)">Más información</a>
            </td>
        </tr>
Next

    </table>

@*<div class="jumbotron">
    <h1>ASP.NET</h1>
    <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS and JavaScript.</p>
    <p><a href="http://asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
</div>

<div class="row">
    <div class="col-md-4">
        <h2>Getting started</h2>
        <p>
            ASP.NET MVC gives you a powerful, patterns-based way to build dynamic websites that
            enables a clean separation of concerns and gives you full control over markup
            for enjoyable, agile development.
        </p>
        <p><a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301865">Learn more &raquo;</a></p>
    </div>
    <div class="col-md-4">
        <h2>Get more libraries</h2>
        <p>NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.</p>
        <p><a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301866">Learn more &raquo;</a></p>
    </div>
    <div class="col-md-4">
        <h2>Web Hosting</h2>
        <p>You can easily find a web hosting company that offers the right mix of features and price for your applications.</p>
        <p><a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301867">Learn more &raquo;</a></p>
    </div>
</div>*@
