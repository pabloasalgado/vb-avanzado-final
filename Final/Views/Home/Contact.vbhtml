﻿@Code
    ViewData("Title") = "Desarrollado por"
End Code

<h2>@ViewData("Title"):</h2>
<h3>@ViewData("Message")</h3>

<address>
    Pablo Salgado<br />
    psalgado@unadvirtual.edu.co<br />
    <abbr title="Código">Cód:</abbr>
    79535939
</address>

<address>
    Carlos Alberto García Parra<br />
    <abbr title="Código">Cód:</abbr>
    79415249
</address>

<address>
    Elkin Gutiérrez Piñeres<br />
    <abbr title="Código">Cód:</abbr>
    79576523
</address>

@*<address>
    <strong>Support:</strong>   <a href="mailto:Support@example.com">Support@example.com</a><br />
    <strong>Marketing:</strong> <a href="mailto:Marketing@example.com">Marketing@example.com</a>
</address>*@
