﻿@ModelType Final.Sitio
@Code
    ViewData("Title") = "Delete"
End Code

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<div>
    <h4>Sitio</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.Ciudad.Nombre)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Ciudad.Nombre)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Nombre)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Nombre)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.FotoUrl)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.FotoUrl)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.InformacionUrl)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.InformacionUrl)
        </dd>

    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
            <input type="submit" value="Delete" class="btn btn-default" /> |
            @Html.ActionLink("Back to List", "Index")
        </div>
    End Using
</div>
