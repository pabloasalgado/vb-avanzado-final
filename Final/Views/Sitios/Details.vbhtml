﻿@ModelType Final.Sitio
@Code
    ViewData("Title") = "Detalles"
End Code

<h2>@Html.DisplayFor(Function(model) model.Nombre)</h2>

<div>
    <h4>@Html.DisplayFor(Function(model) model.Ciudad.Nombre), @Html.DisplayFor(Function(model) model.Ciudad.Pais.Nombre)</h4>
    <hr />
    <img src="/@Model.FotoUrl" style="width: 100%;" alt="@Model.FotoAlt" />
    <span style="font-size: xx-small;">@Html.DisplayFor(Function(model) model.FotoAlt)</span>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            Ciudad
        </dt>

        <dd>            
            @Html.DisplayFor(Function(model) model.Ciudad.Nombre)
        </dd>

        <dt>
            País
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Ciudad.Pais.Nombre)
        </dd>

        <dt>
            Más información
        </dt>

        <dd>
            <a href="@model.InformacionUrl">@Html.DisplayFor(Function(model) model.InformacionUrl)</a>           
        </dd>

    </dl>
</div>
<h2>Hoteles</h2>
<table class="table">
    <tr>
        <th>Nombre</th>
        <th>Estrellas</th>
        <th>Precio</th>
        <th></th>
    </tr>
@For Each item In Model.Hoteles
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Nombre)
        </td>
         <td>
             @Html.DisplayFor(Function(modelItem) item.Estrellas)
         </td>
         <td>
             @Html.DisplayFor(Function(modelItem) item.Precio)
         </td>
         <td>
             <img src="@Url.Content(item.FotoUrl)" style="width: 100px;" />
         </td>
        <td>
            @Html.ActionLink("Reservar", "Create", "Reservas", New With {.hotelId = item.Id}, Nothing)
        </td>
    </tr>
Next

</table>
<p>
    @*@Html.ActionLink("Editar", "Edit", New With {.id = Model.Id}) |*@
    @Html.ActionLink("Nueva búsqueda", "Index", "Home")
</p>
