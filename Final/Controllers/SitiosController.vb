﻿Imports System.Data.Entity
Imports System.Net

Namespace Controllers
    Public Class SitiosController
        Inherits Controller

        Private ReadOnly db As New Model

        ' GET: Sitios
        Function Index() As ActionResult
            Dim sitios = db.Sitios.Include(Function(s) s.Ciudad)
            Return View(sitios.ToList())
        End Function

        ' GET: Sitios/Details/5
        Function Details(id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim sitio As Sitio = db.Sitios.Include(Function(s) s.Ciudad).Include(Function(p) p.Hoteles).FirstOrDefault(Function(x) x.Id = id)
            If IsNothing(sitio) Then
                Return HttpNotFound()
            End If
            Return View(sitio)
        End Function

        ' GET: Sitios/Create
        Function Create() As ActionResult
            ViewBag.CiudadId = New SelectList(db.Ciudades, "Id", "Nombre")
            Return View()
        End Function

        ' POST: Sitios/Create
        'To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        'more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        <HttpPost>
        <ValidateAntiForgeryToken>
        Function Create(<Bind(Include:="Id,Nombre,CiudadId,FotoUrl,InformacionUrl")> sitio As Sitio) As ActionResult
            If ModelState.IsValid Then
                db.Sitios.Add(sitio)
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            ViewBag.CiudadId = New SelectList(db.Ciudades, "Id", "Nombre", sitio.CiudadId)
            Return View(sitio)
        End Function

        ' GET: Sitios/Edit/5
        Function Edit(id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim sitio As Sitio = db.Sitios.Find(id)
            If IsNothing(sitio) Then
                Return HttpNotFound()
            End If
            ViewBag.CiudadId = New SelectList(db.Ciudades, "Id", "Nombre", sitio.CiudadId)
            Return View(sitio)
        End Function

        ' POST: Sitios/Edit/5
        'To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        'more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        <HttpPost>
        <ValidateAntiForgeryToken>
        Function Edit(<Bind(Include:="Id,Nombre,CiudadId,FotoUrl,InformacionUrl")> sitio As Sitio) As ActionResult
            If ModelState.IsValid Then
                db.Entry(sitio).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            ViewBag.CiudadId = New SelectList(db.Ciudades, "Id", "Nombre", sitio.CiudadId)
            Return View(sitio)
        End Function

        ' GET: Sitios/Delete/5
        Function Delete(id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim sitio As Sitio = db.Sitios.Find(id)
            If IsNothing(sitio) Then
                Return HttpNotFound()
            End If
            Return View(sitio)
        End Function

        ' POST: Sitios/Delete/5
        <HttpPost>
        <ActionName("Delete")>
        <ValidateAntiForgeryToken>
        Function DeleteConfirmed(id As Integer) As ActionResult
            Dim sitio As Sitio = db.Sitios.Find(id)
            db.Sitios.Remove(sitio)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace
