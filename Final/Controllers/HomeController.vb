﻿Imports System.Data.Entity

Namespace Controllers
    Public Class HomeController
        Inherits Controller

        Private ReadOnly _db As New Model

        Function Index(searchString As String) As ActionResult
            Dim sitios = _db.Sitios.Include(Function(c) c.Ciudad).Include(Function(c) c.Ciudad.Pais)

            Dim qry = From sit In sitios
                      Where sit.Nombre.Contains(searchString) Or sit.Ciudad.Nombre.Contains(searchString) Select sit

            Return View(qry.ToList())
        End Function

        Function About() As ActionResult
            ViewData("Message") = "Final"

            Return View()
        End Function

        Function Contact() As ActionResult
            ViewData("Message") = "Grupo 405021_3"

            Return View()
        End Function
    End Class
End Namespace