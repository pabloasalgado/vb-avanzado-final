﻿Imports System.Data.Entity
Imports System.Net
Imports Microsoft.AspNet.Identity

Namespace Controllers
    Public Class ReservasController
        Inherits Controller

        Private ReadOnly db As New Model

        ' GET: Reservas
        Function Index() As ActionResult
            Dim reservas = db.Reservas.Include(Function(r) r.Hotel).Include(Function(r) r.Persona)
            Return View(reservas.ToList())
        End Function

        ' GET: Reservas/Details/5
        Function Details(id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim reserva As Reserva = db.Reservas.Find(id)
            If IsNothing(reserva) Then
                Return HttpNotFound()
            End If
            Return View(reserva)
        End Function

        ' GET: Reservas/Create
        Function Create(hotelId As Integer) As ActionResult
            ViewBag.HotelId = New SelectList(db.Hoteles, "Id", "Nombre")
            ViewBag.PersonaId = New SelectList(db.Personas, "Id", "Nombre")

            ' El usuario que hace la reserva
            Dim userName = User.Identity.GetUserName()
            Dim persona = db.Personas.First(Function(p) p.UserName = userName)
            Dim hotel = db.Hoteles.First(Function(p) p.Id = hotelId)
            Dim reserva = New Reserva() _
                    With {.Persona = persona,
                    .PersonaId = persona.Id,
                    .Desde = DateTime.Now,
                    .Hasta = DateTime.Now.AddDays(1),
                    .Hotel = hotel,
                    .HotelId = hotel.Id,
                    .Habitaciones = 1}
            Return View(reserva)
        End Function

        ' POST: Reservas/Create
        'To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        'more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        <HttpPost>
        <ValidateAntiForgeryToken>
        Function Create(<Bind(Include:="Id,PersonaId,HotelId,Desde,Hasta,Total,Habitaciones")> reserva As Reserva) _
            As ActionResult
            If ModelState.IsValid Then
                ' Calcular el total de la reseva contando los días
                Dim hotel = db.Hoteles.First(Function(p) p.Id = reserva.HotelId)
                Dim dias = reserva.Hasta - reserva.Desde
                reserva.Total = hotel.Precio * dias.Days * reserva.Habitaciones

                db.Reservas.Add(reserva)
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            ViewBag.HotelId = New SelectList(db.Hoteles, "Id", "Nombre", reserva.HotelId)
            ViewBag.PersonaId = New SelectList(db.Personas, "Id", "Nombre", reserva.PersonaId)
            Return View(reserva)
        End Function

        ' GET: Reservas/Edit/5
        Function Edit(id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim reserva As Reserva = db.Reservas.Find(id)
            If IsNothing(reserva) Then
                Return HttpNotFound()
            End If
            ViewBag.HotelId = New SelectList(db.Hoteles, "Id", "Nombre", reserva.HotelId)
            ViewBag.PersonaId = New SelectList(db.Personas, "Id", "Nombre", reserva.PersonaId)
            Return View(reserva)
        End Function

        ' POST: Reservas/Edit/5
        'To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        'more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        <HttpPost>
        <ValidateAntiForgeryToken>
        Function Edit(<Bind(Include:="Id,PersonaId,HotelId,Desde,Hasta,Total,Habitaciones")> reserva As Reserva) _
            As ActionResult
            If ModelState.IsValid Then
                db.Entry(reserva).State = EntityState.Modified

                ' Calcular el total de la reseva contando los días
                Dim hotel = db.Hoteles.First(Function(p) p.Id = reserva.HotelId)
                Dim dias = reserva.Hasta - reserva.Desde
                reserva.Total = hotel.Precio * dias.Days * reserva.Habitaciones

                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            ViewBag.HotelId = New SelectList(db.Hoteles, "Id", "Nombre", reserva.HotelId)
            ViewBag.PersonaId = New SelectList(db.Personas, "Id", "Nombre", reserva.PersonaId)
            Return View(reserva)
        End Function

        ' GET: Reservas/Delete/5
        Function Delete(id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim reserva As Reserva = db.Reservas.Find(id)
            If IsNothing(reserva) Then
                Return HttpNotFound()
            End If
            Return View(reserva)
        End Function

        ' POST: Reservas/Delete/5
        <HttpPost>
        <ActionName("Delete")>
        <ValidateAntiForgeryToken>
        Function DeleteConfirmed(id As Integer) As ActionResult
            Dim reserva As Reserva = db.Reservas.Find(id)
            db.Reservas.Remove(reserva)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace
