Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Reservations
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Idiomas",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .Nombre = c.String(),
                        .Codigo = c.String(maxLength := 2)
                    }) _
                .PrimaryKey(Function(t) t.Id)
            
            CreateTable(
                "dbo.Personas",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .Nombre = c.String(),
                        .Apellido = c.String(),
                        .CiudadId = c.Int(),
                        .UserName = c.String(maxLength := 256),
                        .Telefono = c.String(),
                        .Direccion = c.String()
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Ciudads", Function(t) t.CiudadId) _
                .Index(Function(t) t.CiudadId)

            CreateTable(
                "dbo.Reservas",
                Function(c) New With
                    {
                        .Id = c.Int(nullable:=False, identity:=True),
                        .PersonaId = c.Int(nullable:=False),
                        .HotelId = c.Int(nullable:=False),
                        .Desde = c.DateTime(nullable:=False),
                        .Hasta = c.DateTime(nullable:=False),
                        .Total = c.Decimal(nullable:=False, precision:=18, scale:=2),
                        .Habitaciones = c.Int(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Hotels", Function(t) t.HotelId, cascadeDelete:=False) _
                .ForeignKey("dbo.Personas", Function(t) t.PersonaId, cascadeDelete:=False) _
                .Index(Function(t) t.PersonaId) _
                .Index(Function(t) t.HotelId)

        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Reservas", "PersonaId", "dbo.Personas")
            DropForeignKey("dbo.Reservas", "HotelId", "dbo.Hotels")
            DropForeignKey("dbo.Personas", "CiudadId", "dbo.Ciudads")
            DropIndex("dbo.Reservas", New String() { "HotelId" })
            DropIndex("dbo.Reservas", New String() { "PersonaId" })
            DropIndex("dbo.Personas", New String() { "CiudadId" })
            DropTable("dbo.Reservas")
            DropTable("dbo.Personas")
            DropTable("dbo.Idiomas")
        End Sub
    End Class
End Namespace
