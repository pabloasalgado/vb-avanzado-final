Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Initial
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Ciudads",
                Function(c) New With
                    {
                        .Id = c.Int(nullable:=False, identity:=True),
                        .Nombre = c.String(),
                        .PaisId = c.Int(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Pais", Function(t) t.PaisId, cascadeDelete:=False) _
                .Index(Function(t) t.PaisId)

            CreateTable(
                "dbo.Pais",
                Function(c) New With
                    {
                        .Id = c.Int(nullable:=False, identity:=True),
                        .Nombre = c.String()
                    }) _
                .PrimaryKey(Function(t) t.Id)

            CreateTable(
                "dbo.Hotels",
                Function(c) New With
                    {
                        .Id = c.Int(nullable:=False, identity:=True),
                        .Nombre = c.String(),
                        .SitioId = c.Int(nullable:=False),
                        .Estrellas = c.Int(nullable:=False),
                        .Precio = c.Decimal(nullable:=False, precision:=18, scale:=2),
                        .Descripcion = c.String(),
                        .InfoUrl = c.String(),
                        .FotoUrl = c.String()
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Sitios", Function(t) t.SitioId, cascadeDelete:=False) _
                .Index(Function(t) t.SitioId)

            CreateTable(
                "dbo.Sitios",
                Function(c) New With
                    {
                        .Id = c.Int(nullable:=False, identity:=True),
                        .Nombre = c.String(),
                        .CiudadId = c.Int(nullable:=False),
                        .FotoUrl = c.String(),
                        .FotoAlt = c.String(),
                        .InformacionUrl = c.String()
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Ciudads", Function(t) t.CiudadId, cascadeDelete:=False) _
                .Index(Function(t) t.CiudadId)

        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Hotels", "SitioId", "dbo.Sitios")
            DropForeignKey("dbo.Sitios", "CiudadId", "dbo.Ciudads")
            DropForeignKey("dbo.Ciudads", "PaisId", "dbo.Pais")
            DropIndex("dbo.Sitios", New String() { "CiudadId" })
            DropIndex("dbo.Hotels", New String() { "SitioId" })
            DropIndex("dbo.Ciudads", New String() { "PaisId" })
            DropTable("dbo.Sitios")
            DropTable("dbo.Hotels")
            DropTable("dbo.Pais")
            DropTable("dbo.Ciudads")
        End Sub
    End Class
End Namespace
