Imports System.Data.Entity.Migrations

Namespace Migrations
    Friend NotInheritable Class Configuration
        Inherits DbMigrationsConfiguration(Of Model)

        Public Sub New()
            AutomaticMigrationsEnabled = False
        End Sub

        Protected Overrides Sub Seed(context As Model)
            ''La lista de idiomas
            'context.Idiomas.AddOrUpdate(
            '    Function(p) p.Codigo,
            '    New Idioma() With {.Codigo = "es", .Nombre = "Espa�ol"},
            '    New Idioma() With {.Codigo = "en", .Nombre = "Ingl�s"},
            '    New Idioma() With {.Codigo = "it", .Nombre = "Italiano"},
            '    New Idioma() With {.Codigo = "ar", .Nombre = "�rabe"}
            ')

            '' La lista de pa�ses
            'context.Paises.AddOrUpdate(
            '    New Pais() With {.Nombre = "Per�"},
            '    New Pais() With {.Nombre = "Emiratos �rabes Unidos"},
            '    New Pais() With {.Nombre = "India"},
            '    New Pais() With {.Nombre = "Espa�a"},
            '    New Pais() With {.Nombre = "Italia"},
            '    New Pais() With {.Nombre = "Irlanda del Norte"},
            '    New Pais() With {.Nombre = "Colombia"}
            '    )
            'context.SaveChanges()

            '' La lista de ciudades donde existen sitios tur�sticos y hoteles
            'context.Ciudades.AddOrUpdate(
            '    New Ciudad() With {.Pais = context.Paises.Single(Function(p) p.Nombre = "Per�"),
            '                                .Nombre = "Cusco"}
            '    )
            'context.Ciudades.AddOrUpdate(
            '    New Ciudad() With {.Pais = context.Paises.Single(Function(p) p.Nombre = "Emiratos �rabes Unidos"),
            '                                .Nombre = "Abu Dhabi"}
            '    )
            'context.Ciudades.AddOrUpdate(
            '    New Ciudad() With {.Pais = context.Paises.Single(Function(p) p.Nombre = "India"),
            '                                .Nombre = "Agra"}
            '    )
            'context.Ciudades.AddOrUpdate(
            '    New Ciudad() With {.Pais = context.Paises.Single(Function(p) p.Nombre = "Espa�a"),
            '                                .Nombre = "C�rdoba"}
            '    )
            'context.Ciudades.AddOrUpdate(
            '    New Ciudad() With {.Pais = context.Paises.Single(Function(p) p.Nombre = "Italia"),
            '                                .Nombre = "Ciudad del Vaticano"}
            '    )
            'context.Ciudades.AddOrUpdate(
            '    New Ciudad() With {.Pais = context.Paises.Single(Function(p) p.Nombre = "Irlanda del Norte"),
            '                                .Nombre = "Antrim"}
            '    )
            'context.Ciudades.AddOrUpdate(
            '    New Ciudad() With {.Pais = context.Paises.Single(Function(p) p.Nombre = "Colombia"),
            '                                .Nombre = "Cartagena de Indias"}
            '    )
            'context.SaveChanges()

            '' La lista de sitios tur�sticos en cada ciudad
            'context.Sitios.AddOrUpdate(
            '    New Sitio() With {.Ciudad = context.Ciudades.Single(Function(p) p.Nombre = "Cusco"),
            '                              .Nombre = "Machu Picchu",
            '                              .InformacionUrl = "https://en.wikipedia.org/wiki/Machu_Picchu",
            '                              .FotoUrl = "images/922px-80_-_Machu_Picchu_-_Juin_2009_-_edit.2.jpg",
            '                              .FotoAlt =
            '                              "By Martin St-Amant (S23678) - Own work, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=8450312"
            '                              }
            '    )
            'context.Sitios.AddOrUpdate(
            '    New Sitio() With {.Ciudad = context.Ciudades.Single(Function(p) p.Nombre = "Abu Dhabi"),
            '                              .Nombre = "Mezquita Sheikh Zayed",
            '                              .InformacionUrl = "https://es.wikipedia.org/wiki/Mezquita_Sheikh_Zayed",
            '                              .FotoUrl = "images/1200px-Interior_of_Main_Hall_in_Sheik_Zayed_Mosque.jpg",
            '                              .FotoAlt =
            '                              "By FritzDaCat - Own cameraPreviously published: First publish on Wikipedia, CC BY-SA 3.0, https://en.wikipedia.org/w/index.php?curid=41433136"
            '                              }
            '    )
            'context.Sitios.AddOrUpdate(
            '    New Sitio() With {.Ciudad = context.Ciudades.Single(Function(p) p.Nombre = "Agra"),
            '                              .Nombre = "Taj Mahal",
            '                              .InformacionUrl = "https://en.wikipedia.org/wiki/Taj_Mahal",
            '                              .FotoUrl = "images/1092px-Taj_Mahal_in_March_2004.jpg",
            '                              .FotoAlt =
            '                              "By Dhirad, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=113235"
            '                              }
            '    )
            'context.Sitios.AddOrUpdate(
            '    New Sitio() With {.Ciudad = context.Ciudades.Single(Function(p) p.Nombre = "C�rdoba"),
            '                              .Nombre = "Catedral y Mezquita, C�rdoba",
            '                              .InformacionUrl =
            '                              "https://es.wikipedia.org/wiki/Mezquita-catedral_de_C%C3%B3rdoba",
            '                              .FotoUrl =
            '                              "images/1200px-Mezquita_de_C�rdoba_desde_el_aire_(C�rdoba,_Espa�a).jpg",
            '                              .FotoAlt =
            '                              "De Toni Castillo Quero - Flickr: [1], CC BY-SA 2.0, https://commons.wikimedia.org/w/index.php?curid=13926840"
            '                              }
            '    )
            'context.Sitios.AddOrUpdate(
            '    New Sitio() With {.Ciudad = context.Ciudades.Single(Function(p) p.Nombre = "Ciudad del Vaticano"),
            '                              .Nombre = "Bas�lica de San Pedro del Vaticano",
            '                              .InformacionUrl = "https://es.wikipedia.org/wiki/Bas%C3%ADlica_de_San_Pedro",
            '                              .FotoUrl = "images/1200px-Saint_Peter's_Basilica_2014.jpg",
            '                              .FotoAlt =
            '                              "De Livioandronico2013 - Trabajo propio, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=35040237"
            '                              }
            '    )
            'context.Sitios.AddOrUpdate(
            '    New Sitio() With {.Ciudad = context.Ciudades.Single(Function(p) p.Nombre = "Antrim"),
            '                              .Nombre = "Calzada del Gigante",
            '                              .InformacionUrl = "https://es.wikipedia.org/wiki/Calzada_del_Gigante",
            '                              .FotoUrl = "images/Causeway-code_poet-4.jpg",
            '                              .FotoAlt =
            '                              "By code poet on flickr. (http://www.flickr.com/photos/alphageek/20005235/) [CC BY-SA 2.0 (http://creativecommons.org/licenses/by-sa/2.0)], via Wikimedia Commons"
            '                              }
            '    )
            'context.Sitios.AddOrUpdate(
            '    New Sitio() With {.Ciudad = context.Ciudades.Single(Function(p) p.Nombre = "Cartagena de Indias"),
            '                              .Nombre = "Castillo San Felipe de Barajas",
            '                              .InformacionUrl =
            '                              "https://es.wikipedia.org/wiki/Castillo_San_Felipe_de_Barajas",
            '                              .FotoUrl = "images/62_-_Carthag�ne_-_D�cembre_2008.jpg",
            '                              .FotoAlt =
            '                              "By Martin St-Amant (S23678) (Own work) [CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0)], via Wikimedia Commons"
            '                              }
            '    )
            'context.Sitios.AddOrUpdate(
            '    New Sitio() With {.Ciudad = context.Ciudades.Single(Function(p) p.Nombre = "Cartagena de Indias"),
            '                              .Nombre = "Islas Corales del Rosario",
            '                              .InformacionUrl = "https://es.wikipedia.org/wiki/Islas_Corales_del_Rosario",
            '                              .FotoUrl = "images/Karibiska_havet_i_Colombia.jpg",
            '                              .FotoAlt =
            '                              "De Segab - Trabajo propio, Dominio p�blico, https://commons.wikimedia.org/w/index.php?curid=7868823"
            '                              }
            '    )
            'context.Sitios.AddOrUpdate(
            '    New Sitio() With {.Ciudad = context.Ciudades.Single(Function(p) p.Nombre = "Cartagena de Indias"),
            '                              .Nombre = "Cerro de La Popa",
            '                              .InformacionUrl = "https://es.wikipedia.org/wiki/Cerro_de_La_Popa",
            '                              .FotoUrl = "images/Cartagena_de_Indias_desde_el_cerro_La_Popa.jpg",
            '                              .FotoAlt =
            '                              "De Norma G�mez - originally posted to Flickr as Pie de Popa e islas de Santa Cruz de Manga, Boca y Castillo Grande y Tierra Bomba vistas desde el, CC BY 2.0, https://commons.wikimedia.org/w/index.php?curid=4374411"
            '                              }
            '    )
            'context.SaveChanges()

            '' La lista de hoteles en cada ciudad
            'context.Hoteles.AddOrUpdate(
            '    New Hotel() With {.Sitio = context.Sitios.Single(Function(p) p.Nombre = "Machu Picchu"),
            '                               .Nombre = "Polo Cusco Suites",
            '                               .Descripcion =
            '                               "Chic d�cor and rooms with city views are offered in Cusco City only 2 blocks from Wanchaq Train Station. Wi-Fi is free and Alejandro Velasco Astete Airport is 1.2 km away. Free airport pick-up is provided.

            'Polo Cusco Suites offers rooms with cable TV, minibars and private bathrooms with bath tubs. Some rooms include spa baths.

            'There is an on-site restaurant with panoramic views. Guests can also enjoy drinks from the bar.

            'Polo Cusco is 10 blocks from the town�s main square. The tour desk can offer tips for getting around the area and there is currency exchange available.

            'This property is also rated for the best value in Cusco! Guests are getting more for their money when compared to other properties in this city.

            'We speak your language!

            'Polo Cusco Suites has been welcoming Booking.com guests since 20 Mar 2013.
            'Hotel Rooms: 45",
            '                               .Estrellas = 3,
            '                               .FotoUrl = "/images/35592158.jpg",
            '                               .InfoUrl =
            '                               "http://www.booking.com/hotel/pe/polocuscosuites.en-gb.html?aid=306395;label=cusco-TlTgkRkD7RsQCkFAvjNwdgS151878454943%3Apl%3Ata%3Ap1620%3Ap2%3Aac%3Aap1t1%3Aneg%3Afi%3Atiaud-146342138710%3Akwd-431568759%3Alp1003659%3Ali%3Adec%3Adm;sid=5f1e90bcfad51c227c9828c1de9bf82c;src=clp;openedLpRecProp=1;ccpi=1",
            '                               .Precio = New Decimal(149212)
            '                               }
            '    )

            'context.Hoteles.AddOrUpdate(
            '    New Hotel() With {.Sitio = context.Sitios.Single(Function(p) p.Nombre = "Machu Picchu"),
            '                               .Nombre = "Hospedaje Tur�stico Arcangel",
            '                               .Descripcion =
            '                               "Only 300 m from Cusco�s main square, Hospedaje Tur�stico Arcangel offers rooms with free Wi-Fi. Guests are greeted with coca tea and breakfast is included.

            'Hospedaje Tur�stico Arcangel has comfortable rooms with cable TV and private bathroom. Some rooms also feature terraces with city views.

            'Inka Museum is 200 m from the hotel and Santa Catalina Convent is 300 m away.

            'Guests can make use of computers with Internet access at the lobby and are only 2 km away from Velasco Astete Airport. Sol Avenue is 500 m away.

            'Cusco City Centre is a great choice for travellers interested in food, history and culture.

            'This is our guests' favourite part of Cusco, according to independent reviews.

            'This property is also rated for the best value in Cusco! Guests are getting more for their money when compared to other properties in this city.

            'Hospedaje Tur�stico Arcangel has been welcoming Booking.com guests since 5 Apr 2013.
            'Guest house: 19 rooms ",
            '                               .Estrellas = Nothing,
            '                               .FotoUrl = "/images/35848117.jpg",
            '                               .InfoUrl =
            '                               "http://www.booking.com/hotel/pe/hospedaje-turastico-san-blas-ii.en-gb.html?aid=306395;label=cusco-TlTgkRkD7RsQCkFAvjNwdgS151878454943%3Apl%3Ata%3Ap1620%3Ap2%3Aac%3Aap1t1%3Aneg%3Afi%3Atiaud-146342138710%3Akwd-431568759%3Alp1003659%3Ali%3Adec%3Adm;sid=5f1e90bcfad51c227c9828c1de9bf82c;src=clp;openedLpRecProp=1;ccpi=1",
            '                               .Precio = New Decimal(150782)
            '                               }
            '    )
            'context.Hoteles.AddOrUpdate(
            '    New Hotel() With {.Sitio = context.Sitios.Single(Function(p) p.Nombre = "Mezquita Sheikh Zayed"),
            '                               .Nombre = "Grand Millennium Al Wahda Abu Dhabi 5-star hotel",
            '                               .Descripcion =
            '                               "",
            '                               .Estrellas = 5,
            '                               .FotoUrl = "/images/75733825.jpg",
            '                               .InfoUrl =
            '                               "http://www.booking.com/hotel/ae/grand-millennium-al-wahda-abu-dhabi.en-gb.html?aid=306395;label=cusco-TlTgkRkD7RsQCkFAvjNwdgS151878454943%3Apl%3Ata%3Ap1620%3Ap2%3Aac%3Aap1t1%3Aneg%3Afi%3Atiaud-146342138710%3Akwd-431568759%3Alp1003659%3Ali%3Adec%3Adm;sid=5f1e90bcfad51c227c9828c1de9bf82c;dest_id=-782066;dest_type=city;dist=0;group_adults=2;room1=A%2CA;sb_price_type=total;srfid=abd2f78db2260077dc7fa624944bd73f467aebd8X1;type=total;ucfs=1&",
            '                               .Precio = New Decimal(0)
            '                               }
            '    )
            'context.Hoteles.AddOrUpdate(
            '    New Hotel() With {.Sitio = context.Sitios.Single(Function(p) p.Nombre = "Taj Mahal"),
            '                               .Nombre = "The Oberoi Amarvilas Agra",
            '                               .Descripcion =
            '                               "",
            '                               .Estrellas = 5,
            '                               .FotoUrl = "/images/47406124.jpg",
            '                               .InfoUrl =
            '                               "",
            '                               .Precio = New Decimal(586038)
            '                               }
            '    )
            'context.Hoteles.AddOrUpdate(
            '    New Hotel() With {.Sitio = context.Sitios.Single(Function(p) p.Nombre = "Catedral y Mezquita, C�rdoba"),
            '                               .Nombre = "Eurostars Conquistador",
            '                               .Descripcion =
            '                               "",
            '                               .Estrellas = Nothing,
            '                               .FotoUrl = "/images/78157291.jpg",
            '                               .InfoUrl =
            '                               "http://www.booking.com/hotel/es/conquistador.en-gb.html?aid=306395;label=cusco-TlTgkRkD7RsQCkFAvjNwdgS151878454943%3Apl%3Ata%3Ap1620%3Ap2%3Aac%3Aap1t1%3Aneg%3Afi%3Atiaud-146342138710%3Akwd-431568759%3Alp1003659%3Ali%3Adec%3Adm;sid=5f1e90bcfad51c227c9828c1de9bf82c;all_sr_blocks=9008602_88181371_0_2_0;checkin=2016-12-31;checkout=2017-01-01;dest_id=-378765;dest_type=city;dist=0;group_adults=2;highlighted_blocks=9008602_88181371_0_2_0;hpos=1;room1=A%2CA;sb_price_type=total;srfid=c54c0bc6537b792da6551955cac9ad7967b978fbX1;type=total;ucfs=1&",
            '                               .Precio = New Decimal(562409)
            '                               }
            '    )

            ''context.Hoteles.AddOrUpdate(
            ''    New Hotel() With {.Sitio = context.Sitios.Single(Function(p) p.Nombre = ""),
            ''                               .Nombre = "",
            ''                               .Descripcion =
            ''                               "",
            ''                               .Estrellas = Nothing,
            ''                               .FotoUrl = "/images/",
            ''                               .InfoUrl =
            ''                               "",
            ''                               .Precio = New Decimal(0)
            ''                               }
            ''    )

            'context.SaveChanges()
        End Sub
    End Class
End Namespace
