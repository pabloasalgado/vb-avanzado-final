Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity

Public Class Model
    Inherits DbContext

    Public Sub New()
        MyBase.New("name=Model")
    End Sub

    Public Overridable Property Paises As DbSet(Of Pais)
    Public Overridable Property Ciudades As DbSet(Of Ciudad)
    Public Overridable Property Hoteles As DbSet(Of Hotel)
    Public Overridable Property Sitios As DbSet(Of Sitio)
    Public Overridable Property Idiomas As DbSet(Of Idioma)
    Public Overridable Property Reservas As DbSet(Of Reserva)
    Public Overridable Property Personas As DbSet(Of Persona)
End Class

Public Class Hotel
    Public Property Id As Int32

    Public Property Nombre As String

    Public Property SitioId As Int32

    Public Overridable Property Sitio As Sitio

    Public Property Estrellas As Int32

    Public Property Precio As Decimal

    Public Property Descripcion As String

    Public Property InfoUrl As String

    Public Property FotoUrl As String
End Class

Public Class Pais
    Public Property Id As Int32

    Public Property Nombre As String
End Class

Public Class Ciudad
    Public Property Id As Int32

    Public Property Nombre As String

    Public Property PaisId As Int32

    Public Overridable Property Pais As Pais
End Class

Public Class Sitio
    Public Property Id As Int32

    Public Property Nombre As String

    Public Property CiudadId As Int32

    Public Property Ciudad As Ciudad

    Public Property FotoUrl As String

    Public Property FotoAlt As String

    Public Property InformacionUrl As String

    Public Property Hoteles As IList(Of Hotel)
End Class

Public Class Reserva
    Public Property Id As Int32

    Public Property PersonaId As Int32

    Public Property Persona As Persona

    Public Property HotelId As Int32

    Public Property Hotel As Hotel

    <DataType(DataType.Date)>
    Public Property Desde As DateTime

    <DataType(DataType.Date)>
    Public Property Hasta As DateTime

    <DisplayFormat(DataFormatString:="{0:C}", ApplyFormatInEditMode:=True)>
    Public Property Total As Decimal

    Public Property Habitaciones As Int32
End Class

Public Class Persona
    Public Property Id As Int32

    Public Property Nombre As String

    Public Property Apellido As String

    <Display(Name:="Ciudad residencia", Prompt:="Ciudad residencia", Description:="Ciudad residencia")>
    Public Property CiudadId As Int32?

    Public Overridable Property Ciudad As Ciudad

    <Column("UserName", TypeName:="nvarchar")>
    <StringLength(256)>
    <Display(Name:="Usuario")>
    Public Property UserName As String

    <Display(Name:="Tel�fono")>
    Public Property Telefono As String

    <Display(Name:="Direcci�n")>
    Public Property Direccion As String
End Class

Public Class Idioma
    Public Property Id As Int32
    Public Property Nombre As String

    <StringLength(2)>
    Public Property Codigo As String
End Class